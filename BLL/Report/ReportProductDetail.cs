﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BLL.Report
{
    public class ReportProductDetail
    {
        public Product Product { get; set; }
        public Dictionary<string, float> LineTime { get; set; }
        public List<Client> Clients { get; }
        public Dictionary<String, float> TotalProductsPurchasedByCustomer { get; set; }

        public ReportProductDetail()
        {
            Clients = new List<Client>();
            LineTime = new Dictionary<String, float>();
            TotalProductsPurchasedByCustomer = new Dictionary<string, float>();
        }


        public Client MostConcurrentClient()
        {
            var max = 0;
            Client _clientWithMorePurchases=null;
            foreach (var item in Clients)
            {
                if (TotalProductsPurchasedByCustomer[item.Indentification] > max) _clientWithMorePurchases = item;

            }
            return _clientWithMorePurchases;
        }

        public void addClient(Client client)
        {
            if (Clients.Where(p => p.Indentification == client.Indentification).FirstOrDefault() == null) Clients.Add(client);
        }

        public void addTotalByClient(Client client, float total)
        {
            float value;
            if (!TotalProductsPurchasedByCustomer.TryGetValue(client.Indentification, out value))
                TotalProductsPurchasedByCustomer.Add(client.Indentification, total);
            else
                TotalProductsPurchasedByCustomer[client.Indentification] = TotalProductsPurchasedByCustomer[client.Indentification] + total;
        }

        public void addLineTime(string time, float total)
        {
            float value;
            if (!LineTime.TryGetValue(time, out value))
                LineTime.Add(time, total);
            else
                LineTime[time] = LineTime[time] + total;
        }



    }
}
