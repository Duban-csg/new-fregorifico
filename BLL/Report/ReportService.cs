﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using DAL;
using Microsoft.EntityFrameworkCore;

namespace BLL.Report
{
    public class ReportService {


        private readonly InvoiceService invoiceService;
        private readonly ClientService clientService;
        List<ReportProductDetail> reportProductDetails { get; set; }
        List<ReportClientDetail> reportClientDetail { get; set; }
        public ReportService(FigorificoContext _context)
        {
            invoiceService = new InvoiceService(_context);
            clientService = new ClientService(_context);   
            reportProductDetails = new List<ReportProductDetail>();
            reportClientDetail = new List<ReportClientDetail>();
        }
        public List<ReportProductDetail> InfoReportProduct() 
        {
            try
            {
                var allInvoices = invoiceService.GetList();
                if (allInvoices.Invoices!=null)
                {
                    foreach (var invoice in allInvoices.Invoices)
                    {
                        foreach (var invoiceDetail in invoice.InvoiceDetails)
                        {
                            if (!IsProductExistInList(invoiceDetail.Product))
                            {
                                reportProductDetails.Add(CreateReportProductDetail(allInvoices.Invoices, invoiceDetail.Product));
                            }
                        }
                    }
                }
                return reportProductDetails;
            }
            catch (Exception) { return null; }

        }

        public List<ReportClientDetail> InfoReportClient()
        {
            try
            {
                this.InfoReportProduct();
                var ResponseClients = clientService.GetConsult();
                if (ResponseClients.Clients != null)
                {
                    foreach (var item in ResponseClients.Clients)
                    {
                        reportClientDetail.Add(CreateReportClientDetail(item));
                    }
                }
                reportClientDetail.Sort((pair1, pair2) => pair2.TotalBuy.CompareTo(pair1.TotalBuy));
                return reportClientDetail;

            }
            catch(Exception) { return null; }
        }

        private Boolean IsProductExistInList(Product product)
        {
            return reportProductDetails.Where(p => p.Product.IdProduct == product.IdProduct).FirstOrDefault() != null;
        }

        private ReportProductDetail CreateReportProductDetail(IList<Invoice> invoices,Product product)
        {
            var newReportProductDetail = new ReportProductDetail();
            foreach (var invoice in invoices)
            {
                var invoiceDetail = invoice.InvoiceDetails
                    .Where(i => i.Product.IdProduct == product.IdProduct)
                    .FirstOrDefault();

                if (invoiceDetail != null) {
                    newReportProductDetail.Product = invoiceDetail.Product;
                    newReportProductDetail.addClient(invoice.Client);
                    newReportProductDetail.addTotalByClient(invoice.Client, invoiceDetail.Quantity );
                    newReportProductDetail.addLineTime(invoice.SaleDate, invoiceDetail.Quantity);
                } 
            }

            return newReportProductDetail;
        }

        private ReportClientDetail CreateReportClientDetail(Client client)
        {
            var newReportClientDetail = new ReportClientDetail();
            newReportClientDetail.client = client;
            newReportClientDetail.invoices = invoiceService.GetListForClient(client).Invoices;
            newReportClientDetail.FillDictionaryProducts();
            newReportClientDetail.calculateTotalInvoices();
            newReportClientDetail.CalculateTotalEarningsEarned();
            newReportClientDetail.SortAndShortenList();
            return newReportClientDetail;
        }




    }

  
}
