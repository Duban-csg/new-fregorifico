﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BLL.Report
{
    public class ReportClientDetail
    {
        public Client client { get; set; }
        public IList<Invoice> invoices  { get; set; }
        public List<DetailCantProduc> listProduct { get; set; }
        public Decimal TotalBuy { get; set; }
        public Decimal TotalEarningsEarned { get; set; }

        public ReportClientDetail()
        {
            invoices = new List<Invoice>();
            listProduct = new List<DetailCantProduc>();
            
        }

        public void FillDictionaryProducts()
        {
            foreach (var item in invoices)
            {
                foreach(var InvoiceDetail in item.InvoiceDetails)
                {
                    var detailProduct = findProductInList(InvoiceDetail.Product.IdProduct);
                    if (detailProduct == null) listProduct.Add(new DetailCantProduc(InvoiceDetail.Quantity, InvoiceDetail.Product));
                    else detailProduct.cantProduct += InvoiceDetail.Quantity;
                }
            }
        }

        private DetailCantProduc findProductInList(string key)
        {
            return listProduct.Where(p=>p.Product.IdProduct== key).FirstOrDefault();
        }

        public void calculateTotalInvoices()
        {
            foreach (var item in invoices)
            {
                TotalBuy += item.Total;
            }
        }

        public void CalculateTotalEarningsEarned()
        {
            decimal TotalInvested = 0;
            foreach (var invoice in invoices)
            {
                foreach (var detailInvoice in invoice.InvoiceDetails)
                {
                    TotalInvested += detailInvoice.Product.PurchasePrice * decimal.Parse(detailInvoice.Quantity.ToString());
                }
            }
            TotalEarningsEarned = TotalBuy-TotalInvested;


        }

        public void SortAndShortenList()
        {
            listProduct.Sort((pair1, pair2) => pair2.cantProduct.CompareTo(pair1.cantProduct));
            listProduct = listProduct.Take(3).ToList();
        }

    }

    public class DetailCantProduc
    {
        public DetailCantProduc(float cantProduct, Product product)
        {
            this.cantProduct = cantProduct;
            Product = product;
        }

        public float cantProduct { get; set; }
        public Product Product { get; set; }
    }
}
