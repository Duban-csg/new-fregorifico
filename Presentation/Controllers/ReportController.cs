﻿using System.Linq;
using System.Collections.Generic;
using BLL.Report;
using DAL;
using Entity;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace Presentation.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly ReportService reportService;

        public ReportController(FigorificoContext figorificoContext)
        {
            this.reportService = new ReportService(figorificoContext);
        }

        [HttpGet("Product")]
        public ActionResult<List<ReportProductDetail>> GetInfoReportProduct()
        {
            var response = this.reportService.InfoReportProduct();
            return response != null ? Ok(response) : BadRequest(new List<ReportProductDetail>());
        }

        [HttpGet("Client")]
        public ActionResult<List<ReportClientDetail>> GetInfoReportClient()
        {
            var response = this.reportService.InfoReportClient();
            return response != null ? Ok(response) : BadRequest(new List<ReportProductDetail>());
        }
    }
}
