import { Component, OnInit } from '@angular/core';
import { ProductReportService } from '../core/services/Report/product-report/product-report.service';

@Component({
  selector: 'app-report-product-component',
  templateUrl: './report-product-component.component.html',
  styleUrls: ['./report-product-component.component.css']
})
export class ReportProductComponentComponent implements OnInit {

  infoReports: any;
  constructor(
    private serviceReport: ProductReportService
  ) { }

  ngOnInit() {
    this.getInfoProduct();
  }

  getInfoProduct() {
    this.serviceReport.getInfoProduct().subscribe(result => {
      if (result != null) {
        this.infoReports = result;
      }
    });
  }

}
