import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HandleHttpErrorService } from '../../../../@base/handle-http-error.service';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientReportService {

  baseUrle: string;

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') baseUrl: string,
    private handleErrorService: HandleHttpErrorService
  ) {
    this.baseUrle = baseUrl;
  }

  getInfoClient(): Observable<any> {
    return this.http.get<any>(this.baseUrle + 'api/Report/Client')
      .pipe(tap(_ => this.handleErrorService.log('productos consultados')),
        catchError(this.handleErrorService.handleError<any>('Error al consultar productos', null))
      );
  }
}
