import { Component, OnInit } from '@angular/core';
import { ClientReportService } from '../core/services/Report/client-report/client-report.service';

@Component({
  selector: 'app-report-client-component',
  templateUrl: './report-client-component.component.html',
  styleUrls: ['./report-client-component.component.css']
})
export class ReportClientComponentComponent implements OnInit {

  infoReports: [];
  llego: string = "No ha llegado";

  constructor(
    private serviceReport: ClientReportService
  ) { }

  ngOnInit() {
    this.getInfoClient();
  }

  getInfoClient() {
    this.serviceReport.getInfoClient().subscribe(result => {
      if (result != null) {
        this.llego = "Ya llego";
        this.infoReports = result;
      }
    });
  }




}
