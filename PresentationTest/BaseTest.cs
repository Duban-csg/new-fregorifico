﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Microsoft.EntityFrameworkCore;

namespace PresentationTest
{
    public class BaseTest
    {
        protected FigorificoContext CreateContex()
        {
            var option = new DbContextOptionsBuilder<FigorificoContext>().UseMySQL("server=localhost;port=3306;database=dbfigorifico-test;uid=root;password=;SslMode = none").Options;
            return new FigorificoContext(option);
        }


    }
}
