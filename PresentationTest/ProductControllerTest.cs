﻿using NUnit.Framework;
using Presentation.Controllers;
using Presentation.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;

namespace PresentationTest
{
    public class ProductControllerTest:BaseTest
    {
        private ProductController controller;
        [SetUp]
        public void Setup()
        {
            controller = new ProductController(this.CreateContex());
        }

        [Test]
        public void RegistroProductoTest()
        {
            var NuevoProducto = new ProductInputModel();
            NuevoProducto.IdProduct = "AV42";
            NuevoProducto.Type = "Carne de Res";
            NuevoProducto.PurchasePrice = 8400;
            NuevoProducto.SalePrice = 9600;
            NuevoProducto.Quantity = 25;
            NuevoProducto.Iva = 19 ;
            NuevoProducto.Description = "este es un producto 100 % natural y no tiene comparación";
            NuevoProducto.Image = "producto.png";
            var response = this.controller.Post(NuevoProducto).Result;
            response.Should().BeOfType<OkObjectResult>("because {0} is set ", typeof(OkObjectResult));
        }

        [Test]
        public void RegistroProductoIcorrectoTest()
        {
            var NuevoProducto = new ProductInputModel();
            NuevoProducto.IdProduct = "AV42";
            NuevoProducto.Type = "Carne de Res";
            NuevoProducto.PurchasePrice = 8400;
            NuevoProducto.SalePrice = 9600;
            NuevoProducto.Quantity = 25;
            NuevoProducto.Iva = 19;
            NuevoProducto.Description = "este es un producto 100 % natural y no tiene comparación";
            NuevoProducto.Image = "producto.png";
            var response = this.controller.Post(NuevoProducto).Result;
            response.Should().BeOfType<BadRequestObjectResult>("because {0} is set ", typeof(BadRequestObjectResult));
        }
    }
}
